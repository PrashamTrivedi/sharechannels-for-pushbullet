package com.celites.sharechannels;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;


public class ChannelsDetailActivity
		extends AppCompatActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);


		setContentView(R.layout.activity_channels_detail);

		Intent intent = getIntent();
		if (intent != null) {
			Uri data = intent.getData();
			String tag = null;
			if (data != null) {
				tag = data.getQueryParameter("tag");

			} else {
				tag = intent.getStringExtra("channel_tag");
			}
			Bundle bundle = new Bundle();
			bundle.putString("channel_tag", tag);
			getSupportFragmentManager().beginTransaction().replace(R.id.fragmentContainer, ChannelsDetailActivityFragment.newInstance(bundle), tag)
			                           .commit();
		}


	}




}
