package com.celites.sharechannels.models;


import com.google.gson.annotations.SerializedName;

/**
 * Created by Prasham on 4/25/2015.
 */

public class SelfUserModel {

	private String iden;

	private float created;

	private float modified;

	private String email;

	@SerializedName("email_normalized")
	private String emailNormalized;

	private String name;

	@SerializedName("image_url")
	private String imageUrl;

	@SerializedName("google_userinfo")
	private GoogleUserInfo googleUserinfo;

	private Preferences preferences;

	/**
	 * @return The iden
	 */
	public String getIden() {
		return iden;
	}

	/**
	 * @param iden
	 * 		The iden
	 */
	public void setIden(String iden) {
		this.iden = iden;
	}

	/**
	 * @return The created
	 */
	public float getCreated() {
		return created;
	}

	/**
	 * @param created
	 * 		The created
	 */
	public void setCreated(float created) {
		this.created = created;
	}

	/**
	 * @return The modified
	 */
	public float getModified() {
		return modified;
	}

	/**
	 * @param modified
	 * 		The modified
	 */
	public void setModified(float modified) {
		this.modified = modified;
	}

	/**
	 * @return The email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email
	 * 		The email
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return The emailNormalized
	 */
	public String getEmailNormalized() {
		return emailNormalized;
	}

	/**
	 * @param emailNormalized
	 * 		The email_normalized
	 */
	public void setEmailNormalized(String emailNormalized) {
		this.emailNormalized = emailNormalized;
	}

	/**
	 * @return The name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 * 		The name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return The imageUrl
	 */
	public String getImageUrl() {
		return imageUrl;
	}

	/**
	 * @param imageUrl
	 * 		The image_url
	 */
	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	/**
	 * @return The googleUserinfo
	 */
	public GoogleUserInfo getGoogleUserinfo() {
		return googleUserinfo;
	}

	/**
	 * @param googleUserinfo
	 * 		The google_userinfo
	 */
	public void setGoogleUserinfo(GoogleUserInfo googleUserinfo) {
		this.googleUserinfo = googleUserinfo;
	}

	/**
	 * @return The preferences
	 */
	public Preferences getPreferences() {
		return preferences;
	}

	/**
	 * @param preferences
	 * 		The preferences
	 */
	public void setPreferences(Preferences preferences) {
		this.preferences = preferences;
	}

}