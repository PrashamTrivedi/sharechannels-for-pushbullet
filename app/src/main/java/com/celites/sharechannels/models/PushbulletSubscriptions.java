package com.celites.sharechannels.models;

/**
 * Created by Prasham on 4/25/2015.
 */
public class PushbulletSubscriptions {


	private boolean active;

	private String iden;

	private float created;

	private float modified;

	private Channel channel;

	/**
	 * @return The active
	 */
	public boolean isActive() {
		return active;
	}

	/**
	 * @param active
	 * 		The active
	 */
	public void setActive(boolean active) {
		this.active = active;
	}

	/**
	 * @return The iden
	 */
	public String getIden() {
		return iden;
	}

	/**
	 * @param iden
	 * 		The iden
	 */
	public void setIden(String iden) {
		this.iden = iden;
	}

	/**
	 * @return The created
	 */
	public float getCreated() {
		return created;
	}

	/**
	 * @param created
	 * 		The created
	 */
	public void setCreated(float created) {
		this.created = created;
	}

	/**
	 * @return The modified
	 */
	public float getModified() {
		return modified;
	}

	/**
	 * @param modified
	 * 		The modified
	 */
	public void setModified(float modified) {
		this.modified = modified;
	}

	/**
	 * @return The channel
	 */
	public Channel getChannel() {
		return channel;
	}

	/**
	 * @param channel
	 * 		The channel
	 */
	public void setChannel(Channel channel) {
		this.channel = channel;
	}
}
