package com.celites.sharechannels.models;

/**
 * Created by Prasham on 4/25/2015.
 */
public class GoogleUserInfo {
	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "ClassPojo [name = " + name + "]";
	}
}
