package com.celites.sharechannels.models;

/**
 * Created by Prasham on 4/25/2015.
 */
public class SubscribeRequest {
	String channel_tag;

	public String getChannel_tag() {
		return channel_tag;
	}

	public void setChannel_tag(String channel_tag) {
		this.channel_tag = channel_tag;
	}
}
