package com.celites.sharechannels.adapters;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.celites.sharechannels.R;
import com.celites.sharechannels.adapters.ChannelsItemAdapter.ChannelsItemHolder;
import com.celites.sharechannels.databinding.LayoutRecentItemsBinding;
import com.celites.sharechannels.models.RecentPush;
import java.util.ArrayList;

/**
 * Created by Prasham on 5/18/2015.
 */
public class ChannelsItemAdapter
		extends Adapter<ChannelsItemHolder> {

	Context context;
	ArrayList<RecentPush> recentPushes;


	public class ChannelsItemHolder
			extends ViewHolder {

		public ChannelsItemHolder(View itemView, LayoutRecentItemsBinding recentItemsBinding) {
			super(itemView);
			this.recentItemsBinding = recentItemsBinding;
		}

		public void setRecentItems(RecentPush recentPush) {
			recentItemsBinding.setRecentPush(recentPush);
		}

		private LayoutRecentItemsBinding recentItemsBinding;
	}

	@Override
	public ChannelsItemHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		LayoutRecentItemsBinding recentItemsBinding = DataBindingUtil.inflate(inflater, R.layout.layout_recent_items, parent, false);
		return new ChannelsItemHolder(recentItemsBinding.getRoot(), recentItemsBinding);
	}

	@Override
	public void onBindViewHolder(ChannelsItemHolder holder, int position) {
		RecentPush recentPush = getItem(position);
		holder.setRecentItems(recentPush);
	}

	@Override
	public int getItemCount() {
		return recentPushes.size();
	}

	public RecentPush getItem(int position) {
		return recentPushes.get(position);
	}

	public ChannelsItemAdapter(Context context, ArrayList<RecentPush> recentPushes) {
		this.context = context;
		this.recentPushes = recentPushes;
		inflater = LayoutInflater.from(context);
	}

	private final LayoutInflater inflater;


}
