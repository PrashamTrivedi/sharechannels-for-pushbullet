package com.celites.sharechannels.adapters;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.PopupMenu.OnMenuItemClickListener;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.ceelites.devutils.ConstantMethods;
import com.ceelites.librariesdemo.FakeDataProvider;
import com.celites.sharechannels.ChannelsDetailActivity;
import com.celites.sharechannels.R;
import com.celites.sharechannels.adapters.ChannelsAdapter.ChannelsHolder;
import com.celites.sharechannels.databinding.ChannelsListBinding;
import com.celites.sharechannels.models.Channel;
import com.celites.sharechannels.models.Contacts;
import com.celites.sharechannels.models.PushbulletSubscriptions;
import com.celites.sharechannels.utils.CircleTransform;
import com.celites.sharechannels.utils.PushRequest;
import com.celites.sharechannels.utils.PushbulletApiService;
import com.celites.sharechannels.utils.Utils;
import java.util.ArrayList;
import rx.Subscriber;

/**
 * Created by Prasham on 5/5/2015.
 */
public class ChannelsAdapter
		extends Adapter<ChannelsHolder> {


	public class ChannelsHolder
			extends ViewHolder {
		ImageView channelLogo;
		ImageButton shareToFriends;

		public ChannelsHolder(View itemView, ChannelsListBinding binding) {
			super(itemView);
			this.binding = binding;
			channelLogo = binding.channelLogo;
			shareToFriends = binding.shareToFriends;

		}

		public void setChannel(Channel channel) {
			binding.setChannel(channel);
		}


		private ChannelsListBinding binding;
	}

	@Override
	public ChannelsHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		LayoutInflater inflater = LayoutInflater.from(parent.getContext());
		ChannelsListBinding binding = DataBindingUtil.inflate(inflater, R.layout.channels_list, parent, false);
		return new ChannelsHolder(binding.getRoot(), binding);
	}

	@Override
	public void onBindViewHolder(final ChannelsHolder holder, int position) {
		PushbulletSubscriptions subscriptions = getItem(position);

		final Channel channel = subscriptions.getChannel();

		holder.setChannel(channel);

		final FakeDataProvider provider = new FakeDataProvider();
		if (!provider.loadFakeData(holder.channelLogo)) {

			Glide.with(context).load(channel.getImage_url()).transform(new CircleTransform(context)).diskCacheStrategy(DiskCacheStrategy.ALL)
			     .placeholder(R.mipmap.ic_launcher).crossFade().into(holder.channelLogo);
		}

		ConstantMethods.applyTintingToImageView(context.getResources(), holder.shareToFriends, android.R.color.black);
		if (contacts != null) {
			holder.shareToFriends.setVisibility(View.VISIBLE);
		}
		holder.shareToFriends.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(final View v) {
				PopupMenu menu = new PopupMenu(context, v);
				for (Contacts contact : contacts) {
					menu.getMenu().add(provider.getFakeData(context, contact.getName(), 2));
				}
				menu.show();

				menu.setOnMenuItemClickListener(new OnMenuItemClickListener() {
					@Override
					public boolean onMenuItemClick(MenuItem item) {
						int index = item.getOrder();
						Contacts contact = contacts.get(index);
						PushRequest request = PushRequest.creteFromDefault();
						request.setTitle(channel.getName());
						request.setEmail(contact.getEmail());
						request.setUrl("http://prashamtrivedi.github.io/ShareChannels/?tag=" + channel.getTag());
						service.sendInvite(request).subscribe(new Subscriber<Void>() {
							@Override
							public void onCompleted() {
								String text = "Subscription sent successfully";
								Snackbar.make(v, text, Snackbar.LENGTH_LONG).show();
							}

							@Override
							public void onError(Throwable e) {
								Log.d("Error", e.getMessage());
							}

							@Override
							public void onNext(Void aVoid) {

							}
						});
						return true;
					}
				});

			}
		});
		holder.itemView.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(context, ChannelsDetailActivity.class);
				intent.putExtra("channel_tag", channel.getTag());
				context.startActivity(intent);
			}
		});
	}

	@Override
	public int getItemCount() {
		return channels.size();
	}

	public PushbulletSubscriptions getItem(int position) {
		return channels.get(position);
	}

	public ChannelsAdapter(Context context, ArrayList<PushbulletSubscriptions> channels) {
		this.context = context;
		this.channels = channels;
		service = Utils.getWebservice(context);

	}

	private Context context;
	private ArrayList<PushbulletSubscriptions> channels;


	public void setContacts(ArrayList<Contacts> contacts) {
		this.contacts = contacts;
		notifyItemRangeChanged(0, getItemCount());
	}

	private ArrayList<Contacts> contacts;
	private final PushbulletApiService service;

}
