package com.celites.sharechannels.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import com.ceelites.devutils.SharedPreferenceUtils;
import com.celites.sharechannels.BuildConfig;
import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.RestAdapter.LogLevel;

/**
 * Created by Prasham on 4/25/2015.
 */
public class Utils {
	public static PushbulletApiService getWebservice(Context context) {
		String authToken = SharedPreferenceUtils.initWith(context).getString("AccessToken", "");
		return Utils.getWebservice(authToken);
	}

	public static PushbulletApiService getWebservice(final String authToken) {
		RequestInterceptor requestInterceptor = new RequestInterceptor() {
			@Override
			public void intercept(RequestFacade request) {
				request.addHeader("Authorization", "Bearer " + authToken);
			}
		};

		LogLevel logLevel = (BuildConfig.DEBUG) ? LogLevel.FULL : LogLevel.NONE;
		RestAdapter restAdapter =
				new RestAdapter.Builder().setEndpoint(PushbulletApiService.PUSHBULLET_API_ENDPOINT).setRequestInterceptor(requestInterceptor)
				                         .setLogLevel(logLevel).build();
		return restAdapter.create(PushbulletApiService.class);
	}

	public static Bitmap drawableToBitmap(Drawable drawable) {
		if (drawable instanceof BitmapDrawable) {
			return ((BitmapDrawable) drawable).getBitmap();
		}

		Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Config.ARGB_8888);
		Canvas canvas = new Canvas(bitmap);
		drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
		drawable.draw(canvas);

		return bitmap;
	}
}
