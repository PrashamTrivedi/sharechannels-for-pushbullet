package com.ceelites.librariesdemo;

import android.content.Context;
import android.view.View;

/**
 * Created by Prasham on 6/30/2015.
 */
public class FakeDataProvider {
	public boolean loadFakeData(View view) {
		return false;
	}

	public String getFakeData(Context context, String defaultValue, int type) {
		String fakeData = defaultValue;

		return fakeData;
	}
}
