package com.ceelites.librariesdemo;

import android.content.Context;
import android.view.View;
import io.kimo.lib.faker.Faker;
import io.kimo.lib.faker.FakerProvider;

/**
 * Created by Prasham on 6/30/2015.
 */
public class FakeDataProvider {
	public static final int LOREM = 1;
	public static final int NAME = 2;
	public static final int NUMBER = 3;
	public static final int PHONE = 4;
	public static final int INTERNET = 5;
	public static final int URL = 6;

	public boolean loadFakeData(View view) {
		Faker.with(view.getContext()).fill(view);
		return true;
	}

	public String getFakeData(Context context, String defaultValue, int type) {
		FakerProvider with = Faker.with(context);
		String fakeData = defaultValue;
		switch (type) {
			case LOREM:
				fakeData = with.Lorem.character();
				break;
			case PHONE:
				fakeData = with.Phone.phoneWithAreaCode();
				break;
			case NAME:
				fakeData = with.Name.fullName();
				break;
			case NUMBER:
				fakeData = String.valueOf(with.Number.digit());
				break;
			case URL:
				fakeData = with.Url.avatar();
				break;
			case INTERNET:
				fakeData = with.Internet.domain();
				break;
			default:
				fakeData = defaultValue;

		}
		return fakeData;
	}


}
